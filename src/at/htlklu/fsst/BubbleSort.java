package at.htlklu.fsst;

public class BubbleSort {

	public static void sort(int[] arr) {
		for (int i = 0; i < arr.length - 1; i++) {
			if (arr[i] > arr[i + 1]) {
				switchElements(arr, i, i + 1);
				i = -1;
			}
		}
	}

	public static void switchElements(int[] arr, int pos1, int pos2) {
		int temp = arr[pos1];
		arr[pos1] = arr[pos2];
		arr[pos2] = temp;
	}
}
