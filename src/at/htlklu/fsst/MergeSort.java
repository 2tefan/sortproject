package at.htlklu.fsst;

import java.util.Arrays;

public class MergeSort {

	public static boolean debugMessages = false;

	public static void sort(int[] arr) {
		doSorting(arr, 0, arr.length - 1);
	}

	public static void sort(int[] arr, boolean _debugMessages) {
		debugMessages = _debugMessages;
		System.out.println("Starting... " + Arrays.toString(arr));
		doSorting(arr, 0, arr.length - 1);
		System.out.println("Finished... " + Arrays.toString(arr));
	}

	public static void doSorting(int[] arr, int start, int end) {
		int length = end - start;
		if (start < end) {
			int center = start + (length / 2);
			if (debugMessages) {
				System.out.printf("Start %d; End: %d; Center:%d%n", start, end, center);
			}

			doSorting(arr, start, center);
			doSorting(arr, center + 1, end);

			mergeArray(arr, start, center, end);
		}
	}

	private static void mergeArray(int[] arr, int start, int center, int end) {

		int start1 = start;
		int start2 = center + 1;
		int end1 = center;
		int end2 = end;

		int length1 = end1 - start1 + 1;
		int length2 = end2 - start2 + 1;
		int[] tempArr = new int[length1 + length2];

		if (debugMessages) {
			System.out.println(Arrays.toString(arr));
			System.out.printf("Subarray1: L:%d; S:%d; E:%d; first: %d%n", length1, start1, end1, arr[start1]);
			System.out.printf("Subarray2: L:%d; S:%d; E:%d; first: %d%n", length2, start2, end2, arr[start2]);
		}

		if (length1 <= 0 || length2 <= 0) {
			return;
		}

		int index1;
		int index2;

		int count1 = 0;
		int count2 = 0;
		int countArr = 0;

		for (int i = 0; i < tempArr.length; i++) {
			index1 = start1 + count1;
			index2 = start2 + count2;

			if (index2 < arr.length && (count1 < length1) && (count2 < length2)) {
				if (arr[index1] < arr[index2]) {
					tempArr[countArr] = arr[index1];
					count1++;
					countArr++;
					// System.out.printf("Count1: %d%n", arr[index1]);

				} else {
					tempArr[countArr] = arr[index2];
					count2++;
					countArr++;
					// System.out.printf("Count2: %d%n", arr[index2]);

				}
			} else {
				break;
			}
		}

		if (debugMessages) {
			System.out.println(Arrays.toString(tempArr));
		}

		while (count1 < length1) {
			index1 = start1 + count1;

			tempArr[countArr] = arr[index1];
			count1++;
			countArr++;
		}

		while (count2 < length2) {
			index2 = start2 + count2;

			tempArr[countArr] = arr[index2];
			count2++;
			countArr++;
		}

		for (int i = 0; i < tempArr.length; i++) {
			arr[start + i] = tempArr[i];
		}
	}
}
