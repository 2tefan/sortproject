package at.htlklu.fsst;

import java.util.Arrays;

public class QuickSort {

	public static boolean debugMessages = false;

	public static void sort(int[] numbersToSort, boolean _debugMessages) {
		debugMessages = _debugMessages;
		doSorting(numbersToSort, 0, numbersToSort.length - 1);
	}

	public static void sort(int[] numbersToSort) {
		doSorting(numbersToSort, 0, numbersToSort.length - 1);
	}

	public static void doSorting(int[] arr, int start, int end) {
		if (start < end) {
			if (debugMessages) {
				System.out.printf("Start: %d; End %d%n", start, end);
			}
			int previousSplitter = sortPart(arr, start, end);

			doSorting(arr, start, previousSplitter - 1);
			doSorting(arr, previousSplitter + 1, end);
		}
	}

	public static int sortPart(int[] arr, int start, int end) {
		int pivotElement = arr[end];
		int splitter = start; // Left of splitter = smaller; Right and itself = bigger

		for (int i = start; i < end; i++) {
			if (debugMessages) {
				printArray(arr, splitter, end, i);
			}
			if (arr[i] <= pivotElement) {
				splitter++;
				switchElements(arr, i, splitter - 1);
			}
		}

		// Swap pivot point with (number on splitter + 1)
		if (debugMessages) {
			System.out.println(Arrays.toString(arr));
		}
		switchElements(arr, splitter, end);
		if (debugMessages) {
			System.out.println(Arrays.toString(arr));
		}
		return splitter;
	}

	public static void switchElements(int[] arr, int pos1, int pos2) {
		int temp = arr[pos1];
		arr[pos1] = arr[pos2];
		arr[pos2] = temp;
	}

	public static void printArray(int[] arr, int splitter, int pivotIndex, int posFor) {
		System.out.print("[");
		for (int i = 0; i < arr.length; i++) {
			if (i == splitter) {
				System.out.printf("|%d, ", arr[i]);
			} else if (i == pivotIndex) {
				System.out.printf("<%d>, ", arr[i]);
			} else if (i == posFor) {
				System.out.printf(">%d<, ", arr[i]);
			} else {
				System.out.printf("%d, ", arr[i]);
			}
		}
		System.out.println("]");
	}

}
