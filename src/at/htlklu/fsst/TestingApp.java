package at.htlklu.fsst;

import java.util.Arrays;
import java.util.Random;

public class TestingApp {

	public static void main(String[] args) {

		int size = 100000; // try smaller numbers first . do not bubble sort big arrays ;)
		int[] zahlen = new int[size];
		fillRandom(zahlen, size);
		//zahlen = new int[] { 38, 27, 43, 3, 9, 82, 10 };

		int[] numbersQuickSort = Arrays.copyOf(zahlen, zahlen.length);
		int[] numbersJavaSort = Arrays.copyOf(zahlen, zahlen.length);
		int[] numbersMergeSort = Arrays.copyOf(zahlen, zahlen.length);
		//int[] numbersBubbleSort = Arrays.copyOf(zahlen, zahlen.length);

		long startTime = System.currentTimeMillis();
		QuickSort.sort(numbersQuickSort);
		long result = System.currentTimeMillis() - startTime;
		System.out.println("QuickSort: " + result + " ms");
		// System.out.println(Arrays.toString(numbersQuickSort));

		startTime = System.currentTimeMillis();
		Arrays.sort(numbersJavaSort);
		result = System.currentTimeMillis() - startTime;
		System.out.println("Java: " + result + " ms");

		startTime = System.currentTimeMillis();
		MergeSort.sort(numbersMergeSort);
		result = System.currentTimeMillis() - startTime;
		System.out.println("Mergesort: " + result + " ms");

		// startTime = System.currentTimeMillis();
		// BubbleSort.sort(numbersBubbleSort);
		// result = System.currentTimeMillis() - startTime;
		// System.out.println("BubbleSort: " + result + " ms");
		// System.out.println(Arrays.toString(numbersBubbleSort));
	}

	private static void fillRandom(int[] arr, int max) {
		Random rng = new Random();
		for (int i = 0; i < arr.length; i++) {
			arr[i] = rng.nextInt(max);
		}
	}
}
